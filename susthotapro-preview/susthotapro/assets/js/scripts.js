
(function ($) {
    "use strict";

    /*================================
    Preloader
    ==================================*/
    var preloader = $('#preloader');
    $(window).on('load', function() {
        preloader.fadeOut('slow', function() { $(this).remove(); });
    });

    /*================================
    stickey Header
    ==================================*/
    $(window).on('scroll', function() {
        var scroll = $(window).scrollTop(),
            mainHeader = $('.header-bottom'),
            mainHeaderTwo = $('.header-bottom-s2');

        if (scroll > 58) {
            mainHeader.addClass("sticky-header");
        } else {
            mainHeader.removeClass("sticky-header");
        }
        if (scroll > 70) {
            mainHeaderTwo.addClass("sticky-header");
        } else {
            mainHeaderTwo.removeClass("sticky-header");
        }
    });

    /*================================
    Show Hide Search Form
    ==================================*/
    $('.hb-search i').on('click', function () {
        $('.offset-search').addClass('show_hide');
        $('.over_lay').addClass('show_hide');
    });

    $('.over_lay').on('click', function () {
        $('.offset-search').removeClass('show_hide');
        $('.over_lay').removeClass('show_hide');
    });

    /*================================
    Owl Carousel
    ==================================*/
    function slider_area() {
        $('.slider-area').owlCarousel({
            margin: 0,
            loop: true,
            autoplay: true,
            autoplayTimeout: 4000,
            nav: false,
            items: 1,
            smartSpeed: 800,
            dots: true
        });
    };
    slider_area();

    function tst_carousel() {
        $('.tst-carousel').owlCarousel({
            loop: true,
            autoplay: true,
            autoplayTimeout: 4000,
            nav: false,
            items: 1,
            smartSpeed: 800
        });
    };
    tst_carousel();

    function team_carousel() {
        $('.team-carousel').owlCarousel({
            loop: true,
            autoplay: true,
            autoplayTimeout: 4000,
            items: 3,
            nav: true,
            dots: false,
            navText: ['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>'],
            smartSpeed: 800,
            responsive: {
                0: {
                    items: 1,
                    margin: 30
                },
                480: {
                    items: 2,
                    margin: 30
                },
                768: {
                    items: 2,
                    margin: 30
                },
                1024: {
                    items: 3,
                    margin: 40
                }
            }
        });
    };
    team_carousel();

    /*================================
    Magnific Popup
    ==================================*/
    $('.expand-video').magnificPopup({
        type: 'iframe',
        gallery: {
            enabled: true
        }
    });

    /*================================
    counter up
    ==================================*/
    $('.counter').counterUp({
        delay: 10,
        time: 1000
    });

    /*================================
    slicknav
    ==================================*/
    $('ul#slickmenu_active').slicknav({
        prependTo: "#mobile_menu"
    });

    /*================================
    Ajax Contact Form
    ==================================*/
    // $('.screen-reader-response').hide();
    // $('form#cf button#submit').on('click', function() {
    //     var fname = $('#fname').val();
    //     var subject = $('#subject').val();
    //     var email = $('#email').val();
    //     var msg = $('#msg').val();
    //     var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;

    //     if (!regex.test(email)) {
    //         alert('Please enter valid email');
    //         return false;
    //     }

    //     fname = $.trim(fname);
    //     subject = $.trim(subject);
    //     email = $.trim(email);
    //     msg = $.trim(msg);

    //     if (fname != '' && email != '' && msg != '') {
    //         var values = "fname=" + fname + "&email=" + email + " &msg=" + msg;
    //         $.ajax({
    //             type: "POST",
    //             url: "mail.php",
    //             data: values,
    //             success: function() {
    //                 $('#fname').val('');
    //                 $('#email').val('');
    //                 $('#msg').val('');

    //                 $('.screen-reader-response').fadeIn().html('<div class="alert alert-success"><strong>Success!</strong> Email has been sent successfully.</div>');
    //                 setTimeout(function() {
    //                     $('.screen-reader-response').fadeOut('slow');
    //                 }, 4000);
    //             }
    //         });
    //     } else {
    //         $('.screen-reader-response').fadeIn().html('<div class="alert alert-danger"><strong>Warning!</strong> Please fillup the informations correctly.</div>')
    //     }
    //     return false;
    // });

})(jQuery);
// google map activation
function initMap() {
    // Styles a map in night mode.
    var map = new google.maps.Map(document.getElementById('google_map'), {
        center: { lat: 40.674, lng: -73.945 },
        scrollwheel: false,
        zoom: 12,
        styles: [{
            "elementType": "geometry",
            "stylers": [{
                "color": "#f5f5f5"
            }]
        },
        {
            "featureType": "poi",
            "elementType": "labels.text",
            "stylers": [{
                "visibility": "off"
            }]
        },
        {
            "featureType": "poi",
            "elementType": "labels.text.fill",
            "stylers": [{
                "color": "#757575"
            }]
        },
        {
            "featureType": "poi.business",
            "stylers": [{
                "visibility": "off"
            }]
        },
        {
            "featureType": "poi.park",
            "elementType": "geometry",
            "stylers": [{
                "color": "#e5e5e5"
            }]
        },
        {
            "featureType": "transit.station",
            "elementType": "geometry",
            "stylers": [{
                "color": "#eeeeee"
            }]
        }
        ]
    });
    var marker = new google.maps.Marker({
        position: map.getCenter(),
        map: map
    });
}